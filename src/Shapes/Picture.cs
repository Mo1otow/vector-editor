﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using VectorEditor.src.controls;

namespace VectorEditor.src.Shapes
{
    [Serializable]
    public sealed class Picture : BaseFigure
    {
        private Image _image;

        public Picture(Stream anImageStream)
        {
            if (anImageStream == null)
            {
                throw new ArgumentNullException(nameof(anImageStream));
            }

            _image = Image.FromStream(anImageStream);
            Points.AddRange(new[]
            {
                PointF.Empty,
                new PointF(_image.Width, 0),
                new PointF(0, _image.Height)
            });
        }

        ~Picture()
        {
            _image.Dispose();
        }

        public override RectangleF Bounds => createBoundRectangle();

        public override void Draw(Graphics graphics)
        {
            graphics.DrawImage(_image, Points.ToArray());
        }

        public override List<Markers> CreateSizeMarkers()
        {
            return new List<Markers>
            {
                new Markers.TopLeftSizeMarker {TargetFigure = this},
                new Markers.TopMiddleSizeMarker {TargetFigure = this},
                new Markers.TopRightSizeMarker {TargetFigure = this},
                new Markers.MiddleRightSizeMarker {TargetFigure = this},
                new Markers.BottomRightSizeMarker {TargetFigure = this},
                new Markers.BottomMiddleSizeMarker {TargetFigure = this},
                new Markers.BottomLeftSizeMarker {TargetFigure = this},
                new Markers.MiddleLeftSizeMarker {TargetFigure = this}
            };
        }

        public override void DrawSizeMarkers(Graphics graphics)
        {
            foreach (var marker in SizeMarkers)
            {
                marker.UpdateLocation();
                marker.Draw(graphics);
            }
        }

        public override bool PointInFigure(PointF point)
        {
            using (var gp = new GraphicsPath())
            {
                gp.AddRectangle(createBoundRectangle());
                return gp.IsVisible(point);
            }
        }

        protected override void AddFigureToGraphicsPath(GraphicsPath gp)
        {
            gp.AddRectangle(createBoundRectangle());
        }

        private RectangleF createBoundRectangle()
        {
            return new RectangleF(Points[0].X, Points[0].Y, Points[1].X, Points[2].Y);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VectorEditor.src.controls;
using static VectorEditor.src.controls.Markers;

namespace VectorEditor.src.Shapes
{
    public interface IVertexSupport
    {
        List<Markers> CreateVertexMarkers();
        void DrawVertexMarkers(Graphics graphics);
        void DrawCustomFigure(Graphics graphics, PointF[] points);
    }

    public interface IRotateSupport
    {
        void RotateAt(float angle, float cx, float cy);
    }

    public interface ISolidFigure
    {

    }


    //Создание базовых примитивов

    [Serializable]
    public abstract class BaseFigure
    {
        //Хранение :
        //точек фигуры
        protected readonly List<PointF> Points = new List<PointF>();
        //размера
        protected readonly List<Markers> SizeMarkers = new List<Markers>();
        //вершины
        protected readonly List<Markers> VertexMarkers = new List<Markers>();

        // Конструктор по умолчанию

        protected BaseFigure()
        {
            stroke = new stroke();
            fill = new fill();
        }

        protected BaseFigure(Point origin, Point offset) : this() { }

        //Базовая точка, по умолчанию верхний- левый угол 
        public virtual PointF Location { get; set; }

        //Размер : ширина и высота 
        public virtual SizeF Size { get; set; }

        //Контур
        public stroke stroke { get; set; }
        //Заливка
        public fill fill { get; set; }

        //Перемещение фигуры
        public virtual void UpdateLocation(PointF offset)
        {
            var pts = GetPoints();

            var oldRect = CalcFocusRect(PointF.Empty, null);

            var newRect = CalcFocusRect(Point.Empty, null);

            for (var i = 0; i < pts.Length; i++)
            {
                pts[i].X = newRect.Left + (pts[i].X - oldRect.Left) / oldRect.Width * newRect.Width;
                pts[i].Y = newRect.Top + (pts[i].Y - oldRect.Top) / oldRect.Height * newRect.Height;
            }
            SetPoints(pts);
            UpdateMarkers();
        }

        public void UpdateMarkers()
        {
            SizeMarkers.Clear();
            SizeMarkers.AddRange(CreateSizeMarkers());
            foreach (var marker in SizeMarkers) marker.UpdateLocation();
            var figure = this as IVertexSupport;
            if (figure == null) return;
            VertexMarkers.Clear();
            VertexMarkers.AddRange(figure.CreateVertexMarkers());
            foreach (var marker in VertexMarkers) marker.UpdateLocation();
        }


        //Проверка попадания точки на отрезок между двумя другими
        public bool PointInRange(PointF p, PointF p1, PointF p2)
        {
            using (var gp = new GraphicsPath())
            {
                gp.AddLine(p1, p2);
                using (var pen = new Pen(Color.Black, stroke.Width * 5f))
                    return gp.IsOutlineVisible(p, pen);
            }
        }


        // Изменение внутреннего массива точек фигуры при работе с маркерами
        public virtual void UpdateSize(PointF offset, Markers marker)
        {
            PointF[] pts;
            if (marker is ISizeMarker)
            {
                //Перемещение границ
                pts = GetPoints();
                var oldRect = CalcFocusRect(PointF.Empty, marker);
                var newRect = CalcFocusRect(PointF.Empty, marker);
                for (var i = 0; i < pts.Length; i++)
                {
                    pts[i].X = newRect.Left + (pts[i].X - oldRect.Left) / oldRect.Width * newRect.Width;
                    pts[i].Y = newRect.Top + (pts[i].Y - oldRect.Top) / oldRect.Height * newRect.Height;
                }

                SetPoints(pts);
                UpdateMarkers();
            }
            else if (marker is VertexLocationMarker) {

                // перемещение узлов
                pts = GetPoints();
                var index = marker.Index;
                if ((index >= 0) && (index < pts.Length))
                {
                    pts[index].X += offset.X;
                    pts[index].Y += offset.Y;
                    SetPoints(pts);
                    UpdateMarkers();
                }
            }
        }
        //Смещение
        public virtual void offset(PointF pf)
        {
            var pts = GetPoints();
            for (var i = 0; i < pts.Length; i++)
            {
                pts[i].X += pf.X;
                pts[i].Y += pf.Y;
            }
            SetPoints(pts);
        }

        //Отражение по вертикали
        public virtual void FlipVertical()
        {
            var rect = Bounds;
            var cx = rect.X + rect.Width * 0.5F;
            var pts = GetPoints();
            for (var i = 0; i < pts.Length; i++)
            {
                if (pts[i].X < cx)
                    pts[i].X += (cx - pts[i].X) * 2F;
                else
                    if (pts[i].X > cx)
                    pts[i].X -= (pts[i].X - cx) * 2F;
            }
            SetPoints(pts);
        }
        //По горизонтали
        public virtual void FlipHorizontal()
        {
            var rect = Bounds;
            var cy = rect.Y + rect.Height * 0.5F;
            var pts = GetPoints();
            for (var i = 0; i < pts.Length; i++)
            {
                if (pts[i].Y < cy)
                    pts[i].Y += (cy - pts[i].Y) * 2F;
                else
                    if (pts[i].Y > cy)
                    pts[i].Y -= (pts[i].Y - cy) * 2F;
            }
            SetPoints(pts);
        }

        //Поворот под углом
        public virtual void Rotate(float angle)
        {
            var figure = this as IRotateSupport;
            if (figure != null)
            {
                var rect = Bounds;
                var cx = rect.X + rect.Width * 0.5F;
                var cy = rect.Y + rect.Height * 0.5F;
                figure.RotateAt(angle, cx, cy);
            }
        }

        // Отрисовка контура для перетаскивания 
        public virtual void DrawFocusFigure(Graphics graphics, PointF offset, Markers marker)
        {
            if (marker == null)
            {
                using (var gp = new GraphicsPath())
                {
                    AddFigureToGraphicsPath(gp);
                    // получаем графический путь
                    var ps = gp.PathPoints;
                    // для всех точек пути
                    for (var i = 0; i < ps.Length; i++)
                    {
                        // делаем смещение
                        ps[i].X += offset.X;
                        ps[i].Y += offset.Y;
                    }
                    var figure = this as IVertexSupport;
                    if (figure != null)
                        figure.DrawCustomFigure(graphics, ps);
                }
            }
            else
                if (marker is VertexLocationMarker)
            {
                // тянут мышкой за маркер, изменяющий положение узла
                using (var gp = new GraphicsPath())
                {
                    AddFigureToGraphicsPath(gp);
                    var ps = gp.PathPoints;
                    var i = marker.Index;
                    if ((i >= 0) && (i < ps.Length))
                    {
                        ps[i].X += offset.X;
                        ps[i].Y += offset.Y;
                        var figure = this as IVertexSupport;
                        if (figure != null)
                            figure.DrawCustomFigure(graphics, ps);
                    }
                }
            }
            else if (marker is ISizeMarker)
            {
                // тянут за размерный маркер
                var ps = GetPoints();
                var oldrect = CalcFocusRect(PointF.Empty, marker);
                var newrect = CalcFocusRect(offset, marker);
                for (var i = 0; i < ps.Length; i++)
                {
                    ps[i].X = newrect.Left + (ps[i].X - oldrect.Left) / oldrect.Width * newrect.Width;
                    ps[i].Y = newrect.Top + (ps[i].Y - oldrect.Top) / oldrect.Height * newrect.Height;
                }
                var figure = this as IVertexSupport;
                if (figure != null)
                    figure.DrawCustomFigure(graphics, ps);
            }
        }

        // Метод рисования фигуры по точкам базового списка
        public abstract void Draw(Graphics graphics);

        public abstract List<Markers> CreateSizeMarkers();

        public abstract void DrawSizeMarkers(Graphics graphics);
        // Метод проверяет принадлежность точки фигуре
        public abstract bool PointInFigure(PointF point);

        protected RectangleF BoundsRect;
        //Свойство для возвра
        public virtual RectangleF Bounds
        {
            get
            {
                //Если фигура узкая по горизонтали
                if (Math.Abs(BoundsRect.Width - 0) < Single.Epsilon)
                {
                    BoundsRect.X -= 2;
                    BoundsRect.Width += 4;
                }

                //Если по вертикали
                if (Math.Abs(BoundsRect.Height - 0) < Single.Epsilon)
                {
                    BoundsRect.Y -= 2;
                    BoundsRect.Height += 4;
                }
                return BoundsRect;
            }
        }


        //Поиск маркера, в разных режимах (изменение узлов, перетаскивание ...)
        public virtual Markers MarkerSelected(PointF pf, bool nodeChanging)
        {
            if (nodeChanging)
            {
                //Режим изменения узлов
                foreach(var marker in VertexMarkers)
                {
                    marker.UpdateLocation();
                    if (marker.InInsidePoint(Point.Ceiling(pf))) return marker;
                }
            }
            else
            {
                //режим изменения размеров или перемещения
                foreach (var marker in SizeMarkers)
                {
                    marker.UpdateLocation();
                    if (marker.InInsidePoint(Point.Ceiling(pf))) return marker;
                }
            }
            return null;
        }

        // Возращаем массив точек фигуры
        public virtual PointF[] GetPoints()
        {
            //Массив точек линии
            var ps = new PointF[Points.Count];
            Points.CopyTo(ps);
            return (ps);
        }

        //Возвращает массив точек из внешнего массива
        public virtual void SetPoints(PointF[] ps)
        {
            Points.Clear();
            Points.AddRange(ps);
        }

        protected abstract void AddFigureToGraphicsPath(GraphicsPath gp);


        //Расчет нового прямоугольника, с учёьтом новой точки, смещения и индекса.
        public virtual RectangleF CalcFocusRect(PointF offset, Markers marker)
        {
            var rect = Bounds;
            var dx = offset.X;
            var dy = offset.Y;
            var dw = dx;
            var dh = dy;

            if (marker == null)
            {
                //перемещение фигуры
                rect.X += dx;
                rect.Y += dy;
            } else if (marker is TopLeftSizeMarker) {
                //Вверх и влево

                if ((rect.Height - dh > 0) && (rect.Width - dw > 0))
                {
                    rect.Width -= dw;
                    rect.Height -= dh;
                    rect.X += dx;
                    rect.Y += dy;
                }
            } else if (marker is TopMiddleSizeMarker)
            {
                // вверх
                if (rect.Height - dh > 0)
                {
                    rect.Height -= dh;
                    rect.Y += dy;
                }

            } else if (marker is TopRightSizeMarker){
                //вверх и вправо
                if ((rect.Height - dh > 0) && (rect.Width + dw > 0))
                {
                    rect.Width += dw;
                    rect.Height -= dh;
                    rect.Y += dy;
                }
            }
            else if (marker is MiddleRightSizeMarker)
            {
                // вправо
                if (rect.Width + dw > 0)
                {
                    rect.Width += dw;
                }
            }
            else if (marker is BottomRightSizeMarker)
            {
                // Вниз и вправо
                if ((rect.Width + dw > 0) && (rect.Height + dh > 0))
                {
                    rect.Width += dw;
                    rect.Height += dh;
                }
            }
            else if (marker is BottomMiddleSizeMarker)
            {
                // вниз
                if (rect.Height + dh > 0)
                {
                    rect.Height += dh;
                }
            }
            else if (marker is BottomLeftSizeMarker)
            {
                // Вниз и влево
                if ((rect.Height + dh > 0) && (rect.Width - dw > 0))
                {
                    rect.Width -= dw;
                    rect.Height += dh;
                    rect.X += dx;
                }
            }
            else if (marker is MiddleLeftSizeMarker)
            {
                // влево
                if (rect.Width - dw > 0)
                {
                    rect.Width -= dw;
                    rect.X += dx;
                }
            }
            return rect;
        }
        public object Clone()
        {
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                return formatter.Deserialize(stream);
            }
        }
    }
}


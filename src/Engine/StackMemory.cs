﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace VectorEditor.src.Engine
{
    [Serializable]
    public class StackMemory
    {
        // глубина стека
        readonly int _stackDepth;

        readonly List<byte[]> _list = new List<byte[]>();

        public StackMemory(int depth)
        {
            _stackDepth = depth;
            if (depth < 1) _stackDepth = 1;
            _list.Clear();
        }
        

        //Помещаем данные в стэк
        public void Push(MemoryStream stream)
        {
            if (_list.Count > _stackDepth) _list.RemoveAt(0);
            _list.Add(stream.ToArray());
        }

        // Очистка стэка
        public void Clear()
        {
            _list.Clear();
        }

        //Количество сохраненных версий в стэке
        public int Count
        {
            get
            {
                return (_list.Count);
            }
        }


        // Извлечение данных из стека
        public void Pop(MemoryStream stream)
        {
            if (_list.Count <= 0) return;
            var buff = _list[_list.Count - 1];
            stream.Write(buff, 0, buff.Length);
            _list.RemoveAt(_list.Count - 1);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VectorEditor.src.controls;
using VectorEditor.src.Shapes;
using static VectorEditor.src.controls.Markers;

namespace VectorEditor.src.Engine
{
    public class FigureSelectedEventArgs : EventArgs
    {
        public BaseFigure FigureSelected { get; set; }
    }
    
    public enum EditorMode
    {
        Selection,
        Dragging,
        AddLine,
        AddPolygon,
        AddRectangle,
        AddSquare,
        AddEllipse,
        AddCircle,
        AddGPITest
    }


    //Класс для хранения фигур
    [Serializable]
    public class Picture
    {
        public stroke DefaultStroke = new stroke();
        public fill DefaultFill = new fill();

        //Сброс редактора
        public void New()
        {
            _fileName = string.Empty;
            _selected.Clear();
            _figures.Clear();
            DefaultStroke = new stroke();
            DefaultFill = new fill();
            _container.Invalidate();
            OnEditorFarConnerUpdated();
        }
        //Местонахождение курсора во время нажатия на ЛКМ
        public Point MouseDownLocation { get; private set; }

        private PointF _pasteOffset = Point.Empty;

        private Point _mouseOffset = Point.Empty;
        private Rectangle _ribbonRect;

        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        // Внешнее контекстное меню для фона редактора
        public ContextMenuStrip BackgoundContextMenu { get; set; }

        // Внешнее контекстное меню фигуры
        public ContextMenuStrip FigureContextMenu { get; set; }

        private bool _controlPressed;
        private bool _altPressed;

        //Список созданных фигур
        private readonly List<BaseFigure> _figures = new List<BaseFigure>();
        //Список выбранных фигур
        private readonly ObservableCollection<BaseFigure> _selected = new ObservableCollection<BaseFigure>();
        // контейнер для рисования фигур
        [NonSerialized]
        private readonly Control _container;
        [NonSerialized]
        private readonly Form _form;
        private bool _vertexChanging;

        // Режим работы редактора
        public EditorMode EditorMode { get; set; }
        // Текущий индекс маркера
        public Markers CurrentMarker { get; private set; }

        // Режим выбора и перетаскивания узловых точек (изменения узлов)
        public bool VertexChanging
        {
            get { return _vertexChanging; }
            set
            {
                _vertexChanging = value;
                _container.Invalidate();
            }
        }

        // Размерный прямоугольник, охватывающий все фигуры
        public RectangleF GetBounds
        {
            get
            {
                using (var gp = new GraphicsPath())
                {
                    foreach (var fgr in _figures)
                        gp.AddRectangle(fgr.Bounds);
                    return gp.GetBounds();
                }
            }
        }

        // Размер всех фигур в совокупности, с учетом смещения от верхнего левого угла
        public RectangleF ClientRectangle
        {
            get
            {
                var rect = GetBounds;
                rect.Width += rect.Left;
                rect.Height += rect.Top;
                rect.Location = PointF.Empty;
                return rect;
            }
        }

        public Picture(Control container)
        {
            _container = container;
            //Подключаем события в контейнере
            _container.MouseDown += ContainerMouseDown;
            _container.MouseMove += ContainerMouseMove;
            _container.MouseUp += ContainerMouseUp;
            _container.Paint += ContainerPaint;

            //Ищем ссылку на форму, где расположен painbox
            var parent = _container.Parent;
            //Ищем пока не найдем форму или пустой perent
            while (!(parent is Form))
            {
                if (parent == null) break;
                parent = parent.Parent;
            }
            _form = parent as Form;
            // если найдена форма
            if (_form != null)
            {
                // то подключим к ней обработчик нажатия клавиш
                _form.KeyDown += FormKeyDown;
                _form.KeyUp += FormKeyUp;
                // включим признак предварительного просмотра нажатия клавиш
                _form.KeyPreview = true;
            }
            // при изменении выбора выключаем режим изменения узлов
            _selected.CollectionChanged += (sender, args) =>
            {
                VertexChanging = false;
            };
            FileChanged = false;
        }

        // Во внешней форме нажата клавиша
        private void FormKeyDown(object sender, KeyEventArgs e)
        {
            // проверяем нажатие Ctrl
            if (e.Control) _controlPressed = true;
            // проверяем нажатие Alt
            if (e.Alt) _altPressed = true;
            float step = 0;
            if (_controlPressed) step = 1;  // точное позиционирование
            if (_altPressed) step = 10;     // быстрое позиционирование
            switch (e.KeyCode)
            {
                case Keys.Up:
                    FileChanged = true;
                    foreach (var fgr in _selected)
                        fgr.offset(new PointF(0, -step));
                    _container.Invalidate();
                    break;
                case Keys.Down:
                    FileChanged = true;
                    foreach (var fgr in _selected)
                        fgr.offset(new PointF(0, step));
                    _container.Invalidate();
                    break;
                case Keys.Left:
                    FileChanged = true;
                    foreach (var fgr in _selected)
                        fgr.offset(new PointF(-step, 0));
                    _container.Invalidate();
                    break;
                case Keys.Right:
                    FileChanged = true;
                    foreach (var fgr in _selected)
                        fgr.offset(new PointF(step, 0));
                    _container.Invalidate();
                    break;
                case Keys.Delete:
                    if ((_selected.Count > 0) &&
                        (MessageBox.Show(@"Delete selected objects?", @"Editor",
                                         MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question,
                                         MessageBoxDefaultButton.Button3) == DialogResult.Yes))
                    {
                        FileChanged = true;
                        foreach (var fgr in _selected) _figures.Remove(fgr);
                        _selected.Clear();
                        GC.Collect();
                        _container.Invalidate();
                    }
                    break;
            }
        }


        // Во внешней форме отпущена клавиша
        private void FormKeyUp(object sender, KeyEventArgs e)
        {
            // проверяем отпускание Ctrl
            if (!e.Control) _controlPressed = false;
            // проверяем отпускание Alt
            if (e.Alt) _altPressed = false;
        }

        // Отражение всех выбранных по горизонтали
        public void FlipHorizontal()
        {
            if (_selected.Count > 0) FileChanged = true;
            foreach (var fgr in _selected) fgr.FlipHorizontal();
            _container.Invalidate();
        }

        // Отражение всех выбранных по вертикали
        public void FlipVertical()
        {
            if (_selected.Count > 0) FileChanged = true;
            foreach (var fgr in _selected) fgr.FlipVertical();
            _container.Invalidate();
        }


        // Установка цвета контура
        public void SetForeColor(Color selcolor)
        {
            if (_selected.Count > 0) FileChanged = true;
            foreach (var fgr in _selected) fgr.stroke.Color = selcolor;
            _container.Invalidate();
        }


        // Установка цвета фона
        public void SetBackColor(Color selcolor)
        {
            if (_selected.Count > 0) FileChanged = true;
            foreach (var fgr in _selected) fgr.fill.Color = selcolor;
            _container.Invalidate();
        }


        // Установка толщины линии
        public void SetPenWidth(int selwidth)
        {
            if (_selected.Count > 0) FileChanged = true;
            foreach (var fgr in _selected) fgr.stroke.Width = selwidth;
            _container.Invalidate();
        }


        // Поворот всех выбранных налево на 90
        public void TurnRightAt90()
        {
            if (_selected.Count > 0) FileChanged = true;
            foreach (var fgr in _selected) fgr.Rotate(90F);
            _container.Invalidate();
        }
        
        // Поворот всех выбранных направо на 90
        public void TurnLeftAt90()
        {
            if (_selected.Count > 0) FileChanged = true;
            foreach (var fgr in _selected) fgr.Rotate(-90F);
            _container.Invalidate();
        }


        // Переместить фигуру ниже всех фигур
        public void SendToBack()
        {
            if (_selected.Count > 0) FileChanged = true;
            foreach (var fgr in _selected)
            {
                _figures.Remove(fgr);
                _figures.Insert(0, fgr);
            }
            _container.Invalidate();
        }


        // Переместить фигуру выше всех фигур
        public void BringToFront()
        {
            if (_selected.Count > 0) FileChanged = true;
            foreach (var fgr in _selected)
            {
                _figures.Remove(fgr);
                _figures.Add(fgr);
            }
            _container.Invalidate();
        }

        // определение типа формата работы с буфером обмена Windows
        [NonSerialized]
        readonly DataFormats.Format _drawsFormat = DataFormats.GetFormat("clipboardVectorFiguresFormat");


        // Вырезать выделенные в буфер обмена
        public void CutSelectedToClipboard()
        {
            FileChanged = true;
            _pasteOffset = Point.Empty;
            var forcopy = _selected.ToList();
            var clipboardDataObject = new DataObject(_drawsFormat.Name, forcopy);
            Clipboard.SetDataObject(clipboardDataObject, false);
            foreach (var fgr in _selected) _figures.Remove(fgr);
            _selected.Clear();
            GC.Collect();
            _container.Invalidate();
        }

        // Копировать выделенные в буфер обмена
        public void CopySelectedToClipboard()
        {
            _pasteOffset = Point.Empty;
            var forcopy = _selected.ToList();
            var clipboardDataObject = new DataObject(_drawsFormat.Name, forcopy);
            Clipboard.SetDataObject(clipboardDataObject, false);
        }


        // Признак возможности вставки данных из буфера обмена
        public bool CanPasteFromClipboard
        {
            get { return Clipboard.ContainsData(_drawsFormat.Name); }
        }

        // Вставка ранее скопированных фигур из буфера обмена
        public void PasteFromClipboardAndSelected()
        {
            if (!Clipboard.ContainsData(_drawsFormat.Name)) return;
            FileChanged = true;
            var clipboardRetrievedObject = Clipboard.GetDataObject();
            if (clipboardRetrievedObject == null) return;
            var pastedObject = (List<BaseFigure>)clipboardRetrievedObject.GetData(_drawsFormat.Name);
            _selected.Clear();
            _pasteOffset = PointF.Add(_pasteOffset, new SizeF(5, 5));
            foreach (var fgr in pastedObject)
            {
                fgr.offset(_pasteOffset);
                _figures.Add(fgr);
                _selected.Add(fgr);
            }
            if (_selected.Count > 0) Focus(_selected[0]);
            _container.Invalidate();
        }


        // Выбрать все фигуры
        public void SelectAllFigures()
        {
            _selected.Clear();
            foreach (var fgr in _figures)
                _selected.Add(fgr);
            _container.Invalidate();
        }



        private bool _fileChanged;
        private string _fileName = string.Empty;
        private Point _pt1;
        private Point _pt2;

        // Признак изменения данных.
        public bool FileChanged
        {
            get
            {
                return (_fileChanged);
            }
            set
            {
                _fileChanged = value;
                PrepareToUndo(_fileChanged);
                PrepareToRedo(false);
            }
        }

        readonly StackMemory _undoStack = new StackMemory(100);
        readonly StackMemory _redoStack = new StackMemory(100);

        // Подготовка к отмене (сохранения состояния)
        private void PrepareToUndo(bool changed)
        {
            if (changed)
            {
                using (var stream = new MemoryStream())
                {
                    SaveToStream(stream);
                    _undoStack.Push(stream);
                }
            }
            else
                _undoStack.Clear();
        }

        // Подготовка к возврату (сохранения состояния)
        private void PrepareToRedo(bool changed)
        {
            if (changed)
            {
                using (var stream = new MemoryStream())
                {
                    SaveToStream(stream);
                    _redoStack.Push(stream);
                }
            }
            else
                _redoStack.Clear();
        }
        //Сохранить фигуры в поток
        private void SaveToStream(Stream stream, List<BaseFigure> listToSave = null)
        {
            var formatter = new BinaryFormatter();
            var list = (listToSave ?? _figures).ToList();
            formatter.Serialize(stream, list);
            stream.Position = 0;
        }

        // Возможность возврата после отмены
        public bool CanRedoChanges
        {
            get { return (_redoStack.Count > 0); }
        }


        // Возврат после отмены
        public void RedoChanges()
        {
            if (!CanRedoChanges) return;
            PrepareToUndo(true);
            _selected.Clear();
            _figures.Clear();
            GC.Collect();
            using (var stream = new MemoryStream())
            {
                _redoStack.Pop(stream);
                var list = LoadFromStream(stream);
                foreach (var fgr in list) _figures.Add(fgr);
            }
            _container.Invalidate();
        }

        // Возможность отмены действий, изменений
        public bool CanUndoChanges
        {
            get { return (_undoStack.Count > 0); }
        }

        // Отмена действий, изменений
        public void UndoChanges()
        {
            if (!CanUndoChanges) return;
            PrepareToRedo(true);
            _selected.Clear();
            _figures.Clear();
            GC.Collect();
            using (var stream = new MemoryStream())
            {
                _undoStack.Pop(stream);
                var list = LoadFromStream(stream);
                foreach (var fgr in list) _figures.Add(fgr);
            }
            _container.Invalidate();
        }

        // Восстановить фигуры из потока в памяти
        private static IEnumerable<BaseFigure> LoadFromStream(Stream stream)
        {
            try
            {
                var formatter = new BinaryFormatter();
                stream.Position = 0;
                return (List<BaseFigure>)formatter.Deserialize(stream);
            }
            catch (SerializationException e)
            {
                Console.WriteLine(@"Failed to deserialize. Reason: " + e.Message);
                throw;
            }
        }

        protected virtual void OnEditorFarConnerUpdated()
        {
            var handler = EditorFarConnerUpdated;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        // обработчик событий выбора фигур
        public event EventHandler<FigureSelectedEventArgs> FigureSelected;

        //Обработчик событий для изменения размера фигур
        public event EventHandler EditorFarConnerUpdated;


        // Поиск верхней фигуры под курсором
        private BaseFigure PointInFigure(PointF location)
        {
            // просматриваем с конца списка, так как последние нарисованные фигуры вверху
            for (var i = _figures.Count - 1; i >= 0; i--)
            {
                // смотрим на все фигуры, начиная с хвоста списка
                var fgr = _figures[i];
                // если точка не попала в фигуру, то берём следующую
                if (!fgr.PointInFigure(location)) continue;
                return fgr;
            }
            return null;
        }


        // Проверка нажатия на маркер в фигуре
        // положительные индексы - это маркеры размеров
        // отрицательные - маркеры узлов
        // ноль - тело фигуры
        private Markers PointInMarker(PointF location, out BaseFigure figure)
        {
            figure = null;
            // проверка нажатия на маркерах
            for (var i = _selected.Count - 1; i >= 0; i--)
            {
                // смотрим на выбранные фигуры, начиная с хвоста списка
                var fgr = _selected[i];
                var found = fgr.MarkerSelected(location, VertexChanging);
                if (found == null) continue;
                figure = fgr;
                return found;
            }
            return null;
        }
        // Нажатие кнопки "мышки" на PaintBox
        private void ContainerMouseDown(object sender, MouseEventArgs e)
        {
            //Первая точка при нажатии кнопки
            MouseDownLocation = _pt1 = _pt2 = e.Location;
            _mouseOffset = Point.Empty;
            _ribbonRect = Rectangle.Empty;
            //Проверка на режим
            if(EditorMode != EditorMode.Selection)
                _selected.Clear(); //Очищаем список если есть выбранные фигуры

            //Поиск маркера в точке нажатия
            BaseFigure fgr;
            CurrentMarker = PointInMarker(e.Location, out fgr);
            // ищем фигуру в точке нажатия
            if (fgr == null) fgr = PointInFigure(e.Location);
            Focus(fgr);
            if (e.Button == MouseButtons.Left)
            {
                _container.Capture = true;
                switch (EditorMode)
                {
                    case EditorMode.Selection:
                        CheckChangeSelection(e.Location);
                        if (fgr != null)
                            EditorMode = EditorMode.Dragging;
                        break;
                }
                return;
            }
            if (e.Button != MouseButtons.Right) return;
            CheckChangeSelection(e.Location);
            // переключаем режим на выбор рамкой
            EditorMode = EditorMode.Selection;
            ShowContextMenu(e.Location);
            // просим перерисовать
            _container.Invalidate();
        }

        // Проверка возможности выбора фигур
        public bool CanSelectFigures
        {
            get { return !VertexChanging && _figures.Count > 0; }
        }
        // Проверка возможности работы, когда выбрана только одна фигура
        public bool CanOneFigureOperation
        {
            get { return !VertexChanging && _selected.Count == 1; }
        }
        // Проверка возможности работы, когда выбрана группа
        public bool CanGroupFigureOperation
        {
            get { return _selected.Count > 1; }
        }
        // Проверка возможности начать изменение узлов
        public bool CanStartNodeChanging
        {
            get { return !VertexChanging && _selected.Count == 1 && _selected[0] is IVertexSupport; }
        }

        // Проверка возможности закончить изменение узлов
        public bool CanStopVertexChanging
        {
            get { return VertexChanging && _selected.Count == 1; }
        }

        // Проверка возможности удаления узла
        public bool CanVertexDeleting
        {
            get
            {
                return CurrentMarker is VertexLocationMarker &&
                    _selected.Count == 1 && _selected[0] is IVertexSupport &&
                    _selected[0].GetPoints().Length > 2;
            }
        }

        
        // Проверка возможности добавления узла
        
        public bool CanVertexAdding
        {
            get
            {
                if (!CanStopVertexChanging) return false;
                var figure = _selected[0];
                if (figure is IVertexSupport)
                {
                    var ps = figure.GetPoints();
                    var pts = new PointF[ps.Length + 1];
                    ps.CopyTo(pts, 0);
                    // замыкание контура фигуры
                    pts[pts.Length - 1].X = pts[0].X;
                    pts[pts.Length - 1].Y = pts[0].Y;
                    for (var i = 1; i < pts.Length; i++)
                    {
                        // поиск сегмента линии, куда бы можно добавить новый узел
                        if (!figure.PointInRange(MouseDownLocation, pts[i - 1], pts[i])) continue;
                        return true;
                    }
                }
                return false;
            }
        }

        
        // Проверка возможности вращения фигуры
        
        public bool CanFigureRotate
        {
            get
            {
                return _selected.Count == 1 && _selected[0] is IRotateSupport &&
                    _selected[0].GetPoints().Length > 2;
            }
        }

        
        // Проверка возможности заливки фигуры
        
        public bool CanFilling
        {
            get { return _selected.Count == 1 && _selected[0] is ISolidFigure; }
        }

        // Вызов контекстного меню
        private void ShowContextMenu(Point location)
        {
            BaseFigure fgr;
            PointInMarker(location, out fgr);
            // ищем фигуру в точке нажатия
            if (fgr == null)
                fgr = PointInFigure(location);
            // есть ли фигура под курсором мышки?
            if (fgr == null) // это не фигура, показываем общее меню
            {
                if (BackgoundContextMenu != null)
                    BackgoundContextMenu.Show(_container, location);
            }
            else
            {
                if (FigureContextMenu != null)
                    FigureContextMenu.Show(_container, location);
            }
        }



        // Перемещение мышки над PaintBox
        private void ContainerMouseMove(object sender, MouseEventArgs e)
        {
            if (MouseDownLocation == e.Location) return;
            // если удерживается левая кнопка и мышка захвачена
            if (e.Button == MouseButtons.Left && _container.Capture)
            {
                // пересчитываем смещение мышки
                _mouseOffset.X = e.X - MouseDownLocation.X;
                _mouseOffset.Y = e.Y - MouseDownLocation.Y;
                // нормализация параметров для прямоугольника выбора
                // в случае, если мы "растягиваем" прямоугольник не только по "главной" диагонали
                _ribbonRect.X = Math.Min(MouseDownLocation.X, e.Location.X);
                _ribbonRect.Y = Math.Min(MouseDownLocation.Y, e.Location.Y);
                // размеры должны быть всегда положительные числа
                _ribbonRect.Width = Math.Abs(MouseDownLocation.X - e.Location.X);
                _ribbonRect.Height = Math.Abs(MouseDownLocation.Y - e.Location.Y);
                _pt1 = MouseDownLocation;
                _pt2 = e.Location;
                // просим перерисовать
                _container.Invalidate();
            }
            if (e.Button != MouseButtons.None) return;
            BaseFigure fgr;
            var marker = PointInMarker(e.Location, out fgr);
            _container.Cursor = marker != null ? marker.Cursor : Cursors.Default;
        }

        // Отпускание кнопки мышки
        private void ContainerMouseUp(object sender, MouseEventArgs e)
        {
            // если мышь была захвачена
            if (!_container.Capture) return;
            // освобождаем захват мышки
            _container.Capture = false;
            // если нажата левая кнопка
            if (e.Button == MouseButtons.Left)
            {
                var rect = _ribbonRect;
                BaseFigure figure;
                switch (EditorMode)
                {
                    case EditorMode.Selection:
                        // нормализация параметров для прямоугольника выбора
                        // добавляем все фигуры, которые оказались охваченными прямоугольником выбора
                        // в список выбранных фигур
                        foreach (var fgr in _figures.Where(fgr => rect.Contains(Rectangle.Ceiling(fgr.Bounds))))
                            _selected.Add(fgr);
                        if (_selected.Count > 0) Focus(_selected[0]);
                        break;
                    // перетаскивание выбранных фигур "мышкой"
                    case EditorMode.Dragging:
                        if (CurrentMarker == null)
                        {
                            FileChanged = true;
                            // перебираем все выделенные фигуры и смещаем
                            foreach (var fgr in _selected)
                                fgr.UpdateLocation(_mouseOffset);
                            _container.Invalidate();
                            OnEditorFarConnerUpdated();
                        }
                        else if (CurrentMarker is ISizeMarker) // тянут за размерный маркер
                        {
                            FileChanged = true;
                            // перебираем все выделенные фигуры и меняем размер
                            foreach (var fgr in _selected)
                                fgr.UpdateSize(_mouseOffset, CurrentMarker);
                            _container.Invalidate();
                            OnEditorFarConnerUpdated();
                        }
                        else if ((CurrentMarker is VertexLocationMarker) && _selected.Count == 1) // тянут за маркер узла
                        {
                            FileChanged = true;
                            var fgr = _selected[0];
                            fgr.UpdateSize(_mouseOffset, CurrentMarker);
                            _container.Invalidate();
                            OnEditorFarConnerUpdated();
                        }
                        break;
                    case EditorMode.AddLine:
                        figure = new Line(MouseDownLocation, _mouseOffset)
                        {
                            stroke = (stroke)DefaultStroke.Clone()
                        };
                        AddFigure(figure);
                        OnEditorFarConnerUpdated();
                        break;
                    case EditorMode.AddPolygon:
                        figure = new Polygon(rect.Location, new Point(rect.Width, rect.Height))
                        {
                            stroke = (stroke)DefaultStroke.Clone(),
                            fill = (fill)DefaultFill.Clone()
                        };
                        AddFigure(figure);
                        OnEditorFarConnerUpdated();
                        break;
                    case EditorMode.AddRectangle:
                        figure = new Rect(rect.Location, new Point(rect.Width, rect.Height))
                        {
                            stroke = (stroke)DefaultStroke.Clone(),
                            fill = (fill)DefaultFill.Clone()
                        };
                        AddFigure(figure);
                        OnEditorFarConnerUpdated();
                        break;
                    case EditorMode.AddSquare:
                        figure = new Square(rect.Location,
                            new Point(Math.Min(rect.Width, rect.Height), Math.Min(rect.Width, rect.Height)))    
                        {
                            stroke = (stroke)DefaultStroke.Clone(),
                            fill = (fill)DefaultFill.Clone()
                        };
                        AddFigure(figure);
                        OnEditorFarConnerUpdated();
                        break;
                    case EditorMode.AddEllipse:
                        figure = new Ellipse(rect.Location, new Point(rect.Width, rect.Height))
                        {
                            stroke = (stroke)DefaultStroke.Clone(),
                            fill = (fill)DefaultFill.Clone()
                        };
                        AddFigure(figure);
                        OnEditorFarConnerUpdated();
                        break;
                    case EditorMode.AddCircle:
                        figure = new Circle(rect.Location,
                            new Point(Math.Min(rect.Width, rect.Height), Math.Min(rect.Width, rect.Height)))
                        {
                            stroke = (stroke)DefaultStroke.Clone(),
                            fill = (fill)DefaultFill.Clone()
                        };
                        AddFigure(figure);
                        OnEditorFarConnerUpdated();
                        break;

                    case EditorMode.AddGPITest:
                        figure = new GPITest(rect.Location,
                            new Point(Math.Min(rect.Width, rect.Height), Math.Min(rect.Width, rect.Height)))
                        {
                            stroke = (stroke)DefaultStroke.Clone(),
                            fill = (fill)DefaultFill.Clone()
                        };
                        AddFigure(figure);
                        OnEditorFarConnerUpdated();
                        break;
                }
            }
            // возвращаем режим
            EditorMode = EditorMode.Selection;
            // обнуление прямоугольника выбора
            _ribbonRect = Rectangle.Empty;
            _pt1 = _pt2 = Point.Empty;
            _container.Invalidate();
        }

        // Добавить фигуру в список
        public void AddFigure(BaseFigure figure)
        {
            FileChanged = true;
            _figures.Add(figure);
            figure.UpdateMarkers();
            _selected.Clear();
            _selected.Add(figure);
            Focus(figure);
            _container.Invalidate();
        }

        // Проверка попадания на фигуру и выбор или отмена выбора фигуры
        private void CheckChangeSelection(Point location)
        {
            BaseFigure fgr;
            PointInMarker(location, out fgr);
            // ищем фигуру в точке нажатия
            if (fgr == null)
                fgr = PointInFigure(location);
            // если фигура найдена
            if (fgr != null)
            {
                // и если нажат Ctrl на клавиатуре
                if (_controlPressed)
                {
                    // если найденная фигура уже была в списке выбранных
                    var foundIndex = _selected.IndexOf(fgr);
                    if (foundIndex >= 0)
                    {
                        // удаление из списка уже выделенного элемента
                        if (_selected.Count > 1) // последний элемент при Ctrl не убирается
                        {
                            Focus(foundIndex == 0 ? _selected[foundIndex + 1] : _selected[foundIndex - 1]);
                            _selected.Remove(fgr);
                        }
                        else
                            Focus(fgr);
                    }
                    else
                    {
                        _selected.Add(fgr); // иначе добавление к списку
                        Focus(fgr);
                    }
                }
                else // работаем без нажатия Ctrl на клавиатуре
                {
                    // если фигуры не было в списке выбранных, то
                    if (!_selected.Contains(fgr))
                    {
                        _selected.Clear(); // очистка списков
                        _selected.Add(fgr); // выделение одного элемента
                        Focus(fgr);
                    }
                }
                // просим перерисовать контейнер
                _container.Invalidate();
            }
            else // никакая фигура не была найдена 
            {
                _selected.Clear(); // очистка списков     
                Focus(null);
                _container.Invalidate();
            }
        }

        public void AddNode()
        {
            if (_selected.Count != 1) return;
            var figure = _selected[0];
            var ps = figure.GetPoints();
            var pts = new PointF[ps.Length + 1];
            ps.CopyTo(pts, 0);
            // замыкание контура фигуры
            pts[pts.Length - 1].X = pts[0].X;
            pts[pts.Length - 1].Y = pts[0].Y;
            FileChanged = true;
            for (var i = 1; i < pts.Length; i++)
            {
                // поиск сегмента линии, куда бы можно добавить новый узел
                if (!figure.PointInRange(MouseDownLocation, pts[i - 1], pts[i])) continue;
                var points = new List<PointF>(figure.GetPoints());
                points.Insert(i, MouseDownLocation);
                figure.SetPoints(points.ToArray());
                figure.UpdateMarkers();
                break;
            }
            _container.Invalidate();
        }

        // Метод удаления выбранного маркера
        public void RemoveNode()
        {
            if (_selected.Count != 1) return;
            if (CurrentMarker == null) return;
            var markerIndex = CurrentMarker.Index;
            var figure = _selected[0];
            var ps = figure.GetPoints();
            if ((ps.Length <= (figure is Polygon ? 3 : 2)) ||
                (markerIndex >= ps.Length - 1)) return;
            FileChanged = true;
            var points = new List<PointF>(figure.GetPoints());
            points.RemoveAt(markerIndex);
            figure.SetPoints(points.ToArray());
            figure.UpdateMarkers();
            _container.Invalidate();
        }

        private BaseFigure _lastFocused;
        private void Focus(BaseFigure figure)
        {
            if (figure == _lastFocused) return;
            _lastFocused = figure;
            OnFigureSelected(new FigureSelectedEventArgs
            {
                FigureSelected = figure
            });
        }

        // Метод инициации события по окончании процесса выбора фигуры
        protected virtual void OnFigureSelected(FigureSelectedEventArgs e)
        {
            // если на событие подписались, то вызываем его
            if (FigureSelected != null)
                FigureSelected(this, e);
        }

        // Метод записи фигур в файл
        public void SaveToFile(string fileName)
        {
            using (var stream = new MemoryStream())
            {
                SaveToStream(stream);
                File.WriteAllBytes(fileName, stream.GetBuffer());
            }
            _fileName = fileName;
            FileChanged = false;
        }

        // Метод загрузки фигур из файла
        public void LoadFromFile(string fileName)
        {
            _fileName = fileName;
            using (var stream = new MemoryStream())
            {
                _selected.Clear();
                var buff = File.ReadAllBytes(fileName);
                stream.Write(buff, 0, buff.Length);
                stream.Position = 0;
                var list = LoadFromStream(stream);
                _figures.Clear();
                foreach (var fig in list) _figures.Add(fig);
            }
            FileChanged = false;
            _container.Invalidate();
            OnEditorFarConnerUpdated();
        }

        public void UpdateStrokeForFocused(stroke stroke)
        {
            if (_selected.Count != 1) return;
            FileChanged = true;
            _selected[0].stroke = (stroke)stroke.Clone();
            _container.Invalidate();
        }

        public void UpdateFillForFocused(fill fill)
        {
            if (_selected.Count != 1) return;
            FileChanged = true;
            _selected[0].fill = (fill)fill.Clone();
            _container.Invalidate();
        }


        // Обработчик события рисования на поверхности контейнера
        private void ContainerPaint(object sender, PaintEventArgs e)
        {
            // рисуем все созданные фигуры
            foreach (var fgr in _figures)
                fgr.Draw(e.Graphics);
            if (EditorMode != EditorMode.Dragging)
            {
                if (VertexChanging)
                    // маркеры узлов рисуем круглыми
                    foreach (var figure in _selected.OfType<IVertexSupport>())
                        figure.DrawVertexMarkers(e.Graphics);
                else
                    // рисуем маркеры размеров у выбранных фигур
                    foreach (var fgr in _selected)
                        fgr.DrawSizeMarkers(e.Graphics);
                if (_ribbonRect.IsEmpty) return;
                // рисуем рамку прямоугольника выбора
                using (var pen = new Pen(Color.Black))
                {
                    switch (EditorMode)
                    {
                        case EditorMode.Selection:
                            pen.DashStyle = DashStyle.Dot;
                            e.Graphics.DrawRectangle(pen, _ribbonRect);
                            break;
                        case EditorMode.AddLine:
                            pen.DashStyle = DashStyle.Solid;
                            e.Graphics.DrawLine(pen, _pt1, _pt2);
                            break;
                        case EditorMode.AddPolygon:
                        case EditorMode.AddRectangle:
                            e.Graphics.FillRectangle(Brushes.White, _ribbonRect);
                            pen.DashStyle = DashStyle.Solid;
                            e.Graphics.DrawRectangle(pen, _ribbonRect);
                            break;
                        case EditorMode.AddSquare:
                            var square = new Rectangle(_ribbonRect.Location,
                                                       new Size(Math.Min(_ribbonRect.Width, _ribbonRect.Height),
                                                                Math.Min(_ribbonRect.Width, _ribbonRect.Height)));
                            e.Graphics.FillRectangle(Brushes.White, square);
                            pen.DashStyle = DashStyle.Solid;
                            e.Graphics.DrawRectangle(pen, square);
                            break;
                        case EditorMode.AddEllipse:
                            e.Graphics.FillEllipse(Brushes.White, _ribbonRect);
                            pen.DashStyle = DashStyle.Solid;
                            e.Graphics.DrawEllipse(pen, _ribbonRect);
                            break;
                        case EditorMode.AddCircle:
                            var circle = new Rectangle(_ribbonRect.Location,
                                                       new Size(Math.Min(_ribbonRect.Width, _ribbonRect.Height),
                                                                Math.Min(_ribbonRect.Width, _ribbonRect.Height)));
                            e.Graphics.FillEllipse(Brushes.White, circle);
                            pen.DashStyle = DashStyle.Solid;
                            e.Graphics.DrawEllipse(pen, circle);
                            break;

                        case EditorMode.AddGPITest:
                            var gpitest = new Rectangle(_ribbonRect.Location,
                                                        new Size(Math.Min(_ribbonRect.Width, _ribbonRect.Height),
                                                                Math.Min(_ribbonRect.Width, _ribbonRect.Height)));
                            e.Graphics.FillEllipse(Brushes.White, gpitest);
                            pen.DashStyle = DashStyle.Solid;
                            e.Graphics.DrawEllipse(pen, gpitest);
                            break;
                    }
                }
                return;
            }
            // при перетаскивании
            foreach (var fgr in _selected)
                fgr.DrawFocusFigure(e.Graphics, _mouseOffset, CurrentMarker);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VectorEditor.src.controls;
using VectorEditor.src.Engine;
using VectorEditor.src.GUI;
using VectorEditor.src.Shapes;

namespace VectorEditor
{ 
    public partial class Vector : Form
    {
        private readonly Picture _editor;
        private BaseFigure _focusedfigure;

        private const string Title = @"Vector";

        public Vector()
        {
            InitializeComponent();
            // создаём хранилище созданных фигур, которое также и рисует их
            _editor = new Picture(pictureBox1)
            {
                BackgoundContextMenu = contextMenuStrip1,
                FigureContextMenu = contextMenuStrip2
            };
            _editor.FigureSelected += EditorFigureSelected;
            _editor.EditorFarConnerUpdated += EditorFarConnerUpdated;
        }

        void EditorFarConnerUpdated(object sender, EventArgs e)
        {
            UpdateCanvasSize();
        }

        private void contextMenuStrip2_Opening(object sender, CancelEventArgs e)
        {
            toolStripSeparator1.Visible = nodeChangeToolStripMenuItem.Visible = _editor.CanStartNodeChanging;
            endNodeChangingToolStripMenuItem.Visible = _editor.CanStopVertexChanging;
            addNodeToolStripMenuItem.Visible = _editor.CanVertexAdding;
            deleteNodeToolStripMenuItem.Visible = _editor.CanVertexDeleting;
            toolStripSeparator7.Visible = transformToolStripMenuItem.Visible = _editor.CanFigureRotate;
            toolStripSeparator4.Visible = circuitToolStripMenuItem.Visible = pouringToolStripMenuItem.Visible = !_editor.CanGroupFigureOperation;
            pouringToolStripMenuItem.Visible = _editor.CanFilling;
        }

        private void nodeChangeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _editor.VertexChanging = true;
        }

        private void endNodeChangingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _editor.VertexChanging = false;
        }

        private void addNodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _editor.AddNode();
        }

        private void deleteNodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _editor.RemoveNode();
        }

        private void bringToFrontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _editor.BringToFront();
        }

        private void bringToBackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _editor.SendToBack();
        }

        private void roundLeftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _editor.TurnLeftAt90();
        }

        private void roundRightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _editor.TurnRightAt90();
        }

        private void flipVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _editor.FlipVertical();
        }

        private void flipHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _editor.FlipHorizontal();
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            pasteToolStripMenuItem1.Enabled = _editor.CanPasteFromClipboard;
        }

        private void pasteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            _editor.PasteFromClipboardAndSelected();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _editor.CutSelectedToClipboard();
        }

        private void copyToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            _editor.CopySelectedToClipboard();
        }

        // Обработчик смены выбора фигур
        void EditorFigureSelected(object sender, FigureSelectedEventArgs e)
        {
            var figure = _focusedfigure = e.FigureSelected;
            if (figure == null) return;
            _editor.DefaultStroke = (stroke)figure.stroke.Clone();
            if (figure is ISolidFigure)
                _editor.DefaultFill = (fill)figure.fill.Clone();
        }

        private void circuitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_focusedfigure == null) return;
            var frm = new StrokeProps(_focusedfigure.stroke);
            if (frm.ShowDialog(this) != DialogResult.OK) return;
            _editor.UpdateStrokeForFocused(frm.stroke);
            _editor.DefaultStroke = (stroke)frm.stroke.Clone();
        }

        private void pouringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_focusedfigure == null) return;
            var frm = new FillProps(_focusedfigure.fill);
            if (frm.ShowDialog(this) != DialogResult.OK) return;
            _editor.UpdateFillForFocused(frm.Fill);
            _editor.DefaultFill = (fill)frm.Fill.Clone();
        }

        private void editToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            selectAllToolStripMenuItem.Enabled = _editor.CanSelectFigures;
            cutToolStripMenuItem1.Enabled = copyToolStripMenuItem.Enabled = _editor.CanOneFigureOperation ||
                                               _editor.CanGroupFigureOperation;
            pasteToolStripMenuItem.Enabled = _editor.CanPasteFromClipboard;
            undoToolStripMenuItem.Enabled = _editor.CanUndoChanges;
            redoToolStripMenuItem.Enabled = _editor.CanRedoChanges;
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _editor.SelectAllFigures();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            toolStripButton4.Enabled = toolStripButton5.Enabled = _editor.CanOneFigureOperation ||
                                               _editor.CanGroupFigureOperation;
            toolStripButton6.Enabled = _editor.CanPasteFromClipboard;
            toolStripButton7.Enabled = _editor.CanUndoChanges;
            toolStripButton8.Enabled = _editor.CanRedoChanges;
            saveToolStripMenuItem.Enabled = toolStripButton3.Enabled = _editor.FileChanged;
            if (SelectBtn.Checked || _editor.EditorMode != EditorMode.Selection) return;
            foreach (ToolStripButton btn in toolStrip2.Items) btn.Checked = false;
            SelectBtn.Checked = true;
        }

        private void Vector_Resize(object sender, EventArgs e)
        {
            UpdateCanvasSize();
        }

        private void UpdateCanvasSize()
        {
            var editrect = _editor.ClientRectangle;
            pictureBox1.Size = Size.Ceiling(editrect.Size);
            var rect = panel1.ClientRectangle;
            if (pictureBox1.Width < rect.Width) pictureBox1.Width = rect.Width;
            if (pictureBox1.Height < rect.Height) pictureBox1.Height = rect.Height;
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _editor.UndoChanges();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _editor.RedoChanges();
        }

        private void dufaultSelectMode_Click(object sender, EventArgs e)
        {
            foreach (ToolStripButton btn in toolStrip2.Items) btn.Checked = false;
            ((ToolStripButton)sender).Checked = true;
            if (LineBtn.Checked)
            {
                _editor.EditorMode = EditorMode.AddLine;
            }
            else if (PolygonBtn.Checked)
            {
                _editor.EditorMode = EditorMode.AddPolygon;
            }
            else if (RectangleBtn.Checked)
            {
                _editor.EditorMode = EditorMode.AddRectangle;
            }
            else if (SquareBtn.Checked)
            {
                _editor.EditorMode = EditorMode.AddSquare;
            }
            else if (ElipseBtn.Checked)
            {
                _editor.EditorMode = EditorMode.AddEllipse;
            }
            else if (CircleBtn.Checked)
            {
                _editor.EditorMode = EditorMode.AddCircle;
            }
            else if (GPITestBtn.Checked)
            {
                _editor.EditorMode = EditorMode.AddGPITest;
            }
            else
            {
                _editor.EditorMode = EditorMode.Selection;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_editor.FileChanged &&
                MessageBox.Show(@"You have unsaved data! Are you sure??",
                                @"Editor",
                                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question,
                                MessageBoxDefaultButton.Button3) == DialogResult.Yes)
                Close();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(_editor.FileName))
            {
                if (saveFileDialog1.ShowDialog(this) != DialogResult.OK) return;
                _editor.SaveToFile(saveFileDialog1.FileName);
                Text = Title + @" - " + _editor.FileName;
            }
            else
                _editor.SaveToFile(_editor.FileName);
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_editor.FileChanged &&
                (!_editor.FileChanged || MessageBox.Show(@"You have unsaved data! Are you sure?",
                                                         @"Editor",
                                                         MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question,
                                                         MessageBoxDefaultButton.Button3) != DialogResult.Yes)) return;
            if (openFileDialog1.ShowDialog(this) != DialogResult.OK) return;
            _editor.LoadFromFile(openFileDialog1.FileName);
            Text = Title + @" - " + _editor.FileName;
        }

        private void createToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_editor.FileChanged &&
                (!_editor.FileChanged || MessageBox.Show(@"You have unsaved data! Are you sure?",
                                                         @"Editor",
                                                         MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question,
                                                         MessageBoxDefaultButton.Button3) != DialogResult.Yes)) return;
            _editor.New();
            Text = Title;
        }

        private void Vector_Load(object sender, EventArgs e)
        {
            Text = Title;
        }

        private void GPITestBtn_Click(object sender, EventArgs e)
        {

        }
    }
}

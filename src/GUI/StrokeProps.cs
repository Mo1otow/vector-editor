﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VectorEditor.src.controls;

namespace VectorEditor.src.GUI
{
    public partial class StrokeProps : Form
    {
        private int _lastColorIndex;
        private stroke _stroke;

        public stroke stroke { get { return (_stroke); } set { _stroke = value; } }

        public StrokeProps(stroke stroke)
        {
            InitializeComponent();

            // получение всех имён доступных типов линий
            comboBox1.Items.Clear();
            comboBox1.Items.AddRange(Utils.GetPenPatternNames());
            comboBox1.SelectedIndex = 1;


            comboBox2.Items.Clear();
            for (var i = 1; i < 61; i++) comboBox2.Items.Add(i.ToString("0"));


            comboBox3.Items.Clear();
            // получение всех имён доступных цветов
            comboBox3.Items.AddRange(Utils.GetAllColorNames());
            // добавление пункта выбора цвета
            comboBox3.Items.Add("Color choose..."); 
            comboBox3.Text = Utils.GetColorNameFromIndex(_lastColorIndex);
            
            _stroke = (stroke)stroke.Clone();
            
            var index = Utils.ColorToIndex(_stroke.Color);
            if (index < 0)
            {
                Utils.AddCustomColor(_stroke.Color);
                comboBox3.Items.Insert(comboBox3.Items.Count - 1, "My color");
                index = comboBox3.Items.Count - 2;
            }
            if (index >= 0) comboBox3.SelectedIndex = index;
            
            trackBar1.Value = 255 - _stroke.Alpha;
            trackBar1.Text = String.Format("{0} %", (int)(trackBar1.Value / 255.0 * 100.0));
            
            comboBox2.SelectedIndex = (int)_stroke.Width - 1;
            
            if (_stroke.DashStyle == DashStyle.Custom)
                comboBox1.SelectedIndex = 0;
            else
                comboBox1.SelectedIndex = (int)_stroke.DashStyle + 1;
            
            comboBox4.Items.Clear();
            // получение всех имён доступных типов соединений линий
            comboBox4.Items.AddRange(Utils.GetLineJoinNames());
            comboBox4.SelectedIndex = (int)_stroke.LineJoin;
        }

        private void comboBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            var cb = (ComboBox)sender;
            var g = e.Graphics;
            // Draw background of the item.
            e.DrawBackground();
            var rect = new Rectangle(e.Bounds.X, e.Bounds.Top, e.Bounds.Width - 1, e.Bounds.Height - 1);
            try
            {
                rect.Inflate(-4, 0);
                if (e.Index == 0)
                    ShowItemText(e, cb, g, rect);
                else
                {
                    using (var p = new Pen(e.ForeColor))
                    {
                        p.Width = 2;
                        p.DashStyle = (DashStyle)(e.Index - 1);
                        g.DrawLine(p, new Point(rect.Left, rect.Top + rect.Height / 2),
                                      new Point(rect.Right, rect.Top + rect.Height / 2));
                    }
                }
            }
            catch { }
            // Draw the focus rectangle if the mouse hovers over an item.
            e.DrawFocusRectangle();

        }

        private static void ShowItemText(DrawItemEventArgs e, ComboBox cb, Graphics g, Rectangle largerect)
        {
            using (var textColor = new SolidBrush(e.ForeColor))
            {
                string showing = cb.Items[e.Index].ToString();
                g.DrawString(showing, cb.Font, textColor, largerect);
            }
        }

        private void comboBox2_DrawItem(object sender, DrawItemEventArgs e)
        {
            var cb = (ComboBox)sender;
            var g = e.Graphics;
            // Draw the background of the item.
            e.DrawBackground();
            var rect = new Rectangle(e.Bounds.X, e.Bounds.Top, e.Bounds.Width, e.Bounds.Height);
            try
            {
                rect.Inflate(-4, 0);
                using (var p = new Pen(e.ForeColor))
                {
                    p.Width = e.Index + 1;
                    g.DrawLine(p, new Point(rect.Left, rect.Top + rect.Height / 2),
                                  new Point(rect.Right, rect.Top + rect.Height / 2));
                    if (e.Index >= 9)
                    {
                        using (var textColor = new SolidBrush(e.BackColor))
                        {
                            rect.Offset(0, 2);
                            string showing = String.Format("{0} dots", cb.Items[e.Index]);
                            g.DrawString(showing, cb.Font, textColor, rect);
                        }
                    }
                }
            }
            catch { }
            // Draw the focus rectangle if the mouse hovers over an item.
            e.DrawFocusRectangle();
        }

        private void comboBox2_MeasureItem(object sender, MeasureItemEventArgs e)
        {
            var cb = (ComboBox)sender;
            if (e.Index < cb.Height - 8)
                e.ItemHeight = cb.Height;
            else
                e.ItemHeight = e.Index + 8;
        }

        private void comboBox3_DrawItem(object sender, DrawItemEventArgs e)
        {
            var cb = (ComboBox)sender;
            var g = e.Graphics;
            var brushColor = Utils.ColorFromIndex(e.Index);
            // Draw background of the item.
            e.DrawBackground();
            var largerect = new Rectangle(e.Bounds.X, e.Bounds.Top, e.Bounds.Width - 1, e.Bounds.Height - 1);
            var colorrect = new Rectangle(4, e.Bounds.Top + 2, e.Bounds.Height - 2, e.Bounds.Height - 5);
            // отрисовка рамки цвета пунктов основеых цветов
            if (Utils.IsNamedColorIndex(e.Index)) 
            {
                using (var brush = new SolidBrush(brushColor))
                    g.FillRectangle(brush, colorrect);
                g.DrawRectangle(Pens.Black, colorrect);
            }
            var textRect = new RectangleF(e.Bounds.X + colorrect.Width + 5, e.Bounds.Y + 1,
                                                 e.Bounds.Width, e.Bounds.Height);
            using (var textColor = new SolidBrush(e.ForeColor))
            {
                if (Utils.IsNamedColorIndex(e.Index))
                {
                    // отрисовка пунктов основных цветов
                    g.DrawString(Utils.GetColorNameFromIndex(e.Index), cb.Font, textColor, textRect);
                }
                // отрисовка пунктов дополнительных цветов
                else if (Utils.IsCustomColorIndex(e.Index))
                {
                    using (var brush = new SolidBrush(brushColor))
                        g.FillRectangle(brush, largerect);
                    using (var pen = new Pen(cb.BackColor))
                        g.DrawRectangle(pen, largerect);
                }
                else // отрисовка последнего пункта: Выбор цвета...
                    g.DrawString(cb.Items[e.Index].ToString(), cb.Font, textColor, largerect);
            }
            // Draw the focus rectangle if the mouse hovers over an item.
            e.DrawFocusRectangle();
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cbox = (ComboBox)sender;
            if (cbox.SelectedIndex == cbox.Items.Count - 1)
            {
                try
                {
                    colorDialog1.Color = Utils.ColorFromIndex(_lastColorIndex);
                    var selIndex = _lastColorIndex;
                    if (colorDialog1.ShowDialog() == DialogResult.OK)
                    {
                        Color selColor = colorDialog1.Color;
                        _stroke.Color = selColor;
                        if (!Utils.FindColor(selColor))
                        {
                            Utils.AddCustomColor(selColor);
                            colorDialog1.CustomColors = Utils.GetCustomColors();
                            comboBox3.Items.Insert(comboBox3.Items.Count - 1, "My Color");
                            comboBox3.SelectedIndex = comboBox3.Items.Count - 2;
                        }
                        else
                            cbox.SelectedIndex = Utils.ColorToIndex(selColor);
                    }
                    else
                        cbox.SelectedIndex = selIndex;
                }
                catch
                { }
            }
            else
            {
                _lastColorIndex = cbox.SelectedIndex;
                cbox.Refresh();
                pictureBox1.Refresh();
            }

        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            trackBar1.Text = String.Format(CultureInfo.InvariantCulture, "{0}",
                (int)(trackBar1.Value / 255.0 * 100.0)) + @" %";
            _stroke.Alpha = 255 - trackBar1.Value;
            pictureBox1.Refresh();
        }

        private void prewiew(object sender, PaintEventArgs e)
        {
            var g = e.Graphics;
            g.SmoothingMode = SmoothingMode.HighQuality;
            var pb = (PictureBox)sender;
            var rect = new RectangleF(0, 0, pb.Width, pb.Height);
            var ps = new PointF[3];
            ps[0] = new PointF(rect.Left + rect.Width / 2, rect.Top + rect.Height / 8);
            ps[1] = new PointF(rect.Left + rect.Width / 4, rect.Top + 7 * rect.Height / 8);
            ps[2] = new PointF(rect.Right - rect.Width / 8, rect.Bottom - rect.Height / 8);
            using (var pen = new Pen(Color.Black))
                g.DrawLines(_stroke.UpdatePen(pen), ps);
        }

        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var cb = (ComboBox)sender;
            if (cb.SelectedIndex == 0) _stroke.DashStyle = DashStyle.Custom;
            else _stroke.DashStyle = (DashStyle)(cb.SelectedIndex - 1);
            pictureBox1.Refresh();
        }

        private void comboBox2_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var cb = (ComboBox)sender;
            _stroke.Width = cb.SelectedIndex + 1;
            pictureBox1.Refresh();
        }

        private void comboBox3_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var cb = (ComboBox)sender;
            _stroke.Color = Utils.ColorFromIndex(cb.SelectedIndex);
            pictureBox1.Refresh();
        }

        private void combobox4(object sender, EventArgs e)
        {
            var cb = (ComboBox)sender;
            _stroke.LineJoin = (LineJoin)(cb.SelectedIndex);
            pictureBox1.Refresh();
        }
    }
}

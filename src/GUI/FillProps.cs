﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VectorEditor.src.controls;
using VectorEditor.src.Engine;

namespace VectorEditor.src.GUI
{
    public partial class FillProps : Form
    {
        int _lastColorIndex;

        private fill _fill;

        public fill Fill { get { return (_fill); } set { _fill = value; } }

        public FillProps(fill fill)
        {
            InitializeComponent();

            comboBox1.Items.Clear();
            // получение всех имён доступных цветов
            comboBox1.Items.AddRange(Utils.GetAllColorNames());
            // добавление пункта выбора цвета
            comboBox1.Items.Add("Color choose...");
            comboBox1.Text = Utils.GetColorNameFromIndex(_lastColorIndex);
            // -------------------------------------------------------------------
            _fill = (fill)fill.Clone();
            // -------------------------------
            var index = Utils.ColorToIndex(fill.Color);
            if (index < 0)
            {
                Utils.AddCustomColor(fill.Color);
                comboBox1.Items.Insert(comboBox1.Items.Count - 1, "My Color");
                index = comboBox1.Items.Count - 2;
            }
            if (index >= 0) comboBox1.SelectedIndex = index;
            // -------------------------------
            trackBar1.Value = 255 - fill.Alpha;
            trackBar1.Text = String.Format(CultureInfo.InvariantCulture, "{0}",
                                                (int)(trackBar1.Value / 255.0 * 100.0)) + @" %";
        }

        private void comboBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            var cb = (ComboBox)sender;
            var g = e.Graphics;
            var brushColor = Utils.ColorFromIndex(e.Index);
            // Draw the background of the item.
            e.DrawBackground();
            var largerect = new Rectangle(e.Bounds.X, e.Bounds.Top, e.Bounds.Width - 1, e.Bounds.Height - 1);
            var colorrect = new Rectangle(4, e.Bounds.Top + 2, e.Bounds.Height - 2, e.Bounds.Height - 5);
            // отрисовка рамки цвета пунктов основеых цветов
            if (Utils.IsNamedColorIndex(e.Index)) 
            {
                using (var brush = new SolidBrush(brushColor))
                {
                    g.FillRectangle(brush, colorrect);
                }
                g.DrawRectangle(Pens.Black, colorrect);
            }
            var textRect = new RectangleF(e.Bounds.X + colorrect.Width + 5, e.Bounds.Y + 1,
                                                 e.Bounds.Width, e.Bounds.Height);
            using (var textColor = new SolidBrush(e.ForeColor))
            {
                if (Utils.IsNamedColorIndex(e.Index))
                {// отрисовка пунктов основных цветов
                    g.DrawString(Utils.GetColorNameFromIndex(e.Index), cb.Font, textColor, textRect);
                }
                // отрисовка пунктов дополнительных цветов
                else if (Utils.IsCustomColorIndex(e.Index)) 
                {
                    using (var brush = new SolidBrush(brushColor))
                    {
                        g.FillRectangle(brush, largerect);
                    }
                    using (var pen = new Pen(cb.BackColor))
                    {
                        g.DrawRectangle(pen, largerect);
                    }
                }
                else // отрисовка последнего пункта: Выбор цвета...
                    g.DrawString(cb.Items[e.Index].ToString(), cb.Font, textColor, largerect);
            }
            // Draw the focus rectangle if the mouse hovers over an item.
            e.DrawFocusRectangle();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            trackBar1.Text = String.Format("{0} %", (int)(trackBar1.Value / 255.0 * 100.0));
            _fill.Alpha = 255 - trackBar1.Value;
            pictureBox1.Refresh();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            var g = e.Graphics;
            var pb = (PictureBox)sender;
            var rect = new RectangleF(0, 0, pb.Width, pb.Height);
            using (var brush = new SolidBrush(Color.White))
                g.FillRectangle(_fill.UpdateBrush(brush), rect);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cbox = (ComboBox)sender;
            if (cbox.SelectedIndex == cbox.Items.Count - 1)
            {
                try
                {
                    colorDialog1.Color = Utils.ColorFromIndex(_lastColorIndex);
                    var selIndex = _lastColorIndex;
                    if (colorDialog1.ShowDialog() == DialogResult.OK)
                    {
                        var selColor = colorDialog1.Color;
                        _fill.Color = selColor;
                        if (!Utils.FindColor(selColor))
                        {
                            Utils.AddCustomColor(selColor);
                            colorDialog1.CustomColors = Utils.GetCustomColors();
                            comboBox1.Items.Insert(comboBox1.Items.Count - 1, "My color");
                            comboBox1.SelectedIndex = comboBox1.Items.Count - 2;
                        }
                        else
                            cbox.SelectedIndex = Utils.ColorToIndex(selColor);
                    }
                    else
                        cbox.SelectedIndex = selIndex;
                }
                catch
                {
                }
            }
            else
            {
                _lastColorIndex = cbox.SelectedIndex;
                cbox.Refresh();
                pictureBox1.Refresh();
            }
        }

        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var cb = (ComboBox)sender;
            _fill.Color = Utils.ColorFromIndex(cb.SelectedIndex);
            pictureBox1.Refresh();
        }
    }
}

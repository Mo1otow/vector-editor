﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace VectorEditor.src.controls
{
    [Serializable]
    public class stroke : ICloneable
    {

        //byDefault
        public stroke()
        {
            Color = Color.Black;
            Width = 1f;
            Alpha = 255;
        }
        //Цвет
        public Color Color { get; set; }
        //Ширина
        public float Width { get; set; }
        //Прозрачность
        public int Alpha { get; set; }
        //Стиль линии
        public DashStyle DashStyle { get; set; }

        //Стиль Начала линии
        public LineCap StartCap { get; set; }
        //Стиль конца линии
        public LineCap EndCap { get; set; }
        //Стиль Объединение линии
        public LineJoin LineJoin { get; set; }

        //Измение обводки
        public Pen UpdatePen(Pen pen)
        {
            if (pen == null)
                throw new ArgumentNullException();
            pen.Color = Color.FromArgb(Alpha, Color);
            pen.Width = Width;
            pen.DashStyle = DashStyle;
            pen.StartCap = StartCap;
            pen.EndCap = EndCap;
            pen.LineJoin = LineJoin;
            return pen;
        }
        public object Clone()
        {
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                return formatter.Deserialize(stream);
            }
        }

    }
}

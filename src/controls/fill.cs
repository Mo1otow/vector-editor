﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace VectorEditor.src.controls
{
    [Serializable]
    public class fill : ICloneable
    {

        //byDefault
        public fill()
        {
            Color = Color.White;
            Alpha = 255;
        }
        
        public Color Color { get; set; }

        //Прозрачность 0 - ниче не видно, 255- максимум
        public int Alpha { get; set; }
        //Смена заливки
        public Brush UpdateBrush(Brush brush)
        {
            if (brush == null) throw new ArgumentNullException();
            var solidBrush = brush as SolidBrush;
            // Обновить цвет
            if (solidBrush != null) solidBrush.Color = Color.FromArgb(Alpha, Color);
            return brush;
        }


        //Копирование 
        public object Clone()
        {
            using (var stream = new MemoryStream())
            {

                //Заносим в память
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                //Вытягиваем
                return formatter.Deserialize(stream);
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VectorEditor.src.Shapes;

namespace VectorEditor.src.controls
{
    //Interface for sizeble marker
    public interface ISizeMarker { }

    [Serializable]
    public abstract class Markers
    {
        //settings byDefault
        protected static int DefSize = 2;

        public BaseFigure TargetFigure;

        public abstract Cursor Cursor { get; }

        public abstract int Index { get; }

        public bool InInsidePoint(Point p)
        {
            if (p.X < Location.X - DefSize || p.X > Location.X + DefSize)
                return false;

            if (p.Y < Location.Y - DefSize || p.Y > Location.Y + DefSize)
                return false;

            return true;
        }
        public abstract void UpdateLocation();

        public virtual PointF Location { get; set; }

        public virtual void Draw(Graphics gr)
        {
            gr.DrawRectangle(Pens.Black, Location.X - DefSize,
                Location.Y - DefSize, DefSize * 2, DefSize * 2);
            gr.FillRectangle(Brushes.Violet, Location.X - DefSize,
                Location.Y - DefSize, DefSize * 2, DefSize * 2);
        }

        [Serializable]
        public class TopLeftSizeMarker : Markers, ISizeMarker
        {
            public override int Index { get { return 1; } }

            public override Cursor Cursor { get { return Cursors.SizeNWSE; } }
            public override void UpdateLocation()
            {
                if (TargetFigure == null) return;
                var bounds = TargetFigure.Bounds;
                Location = new Point((int)Math.Round(bounds.Left) - DefSize / 2,
                                    (int)Math.Round(bounds.Top) - DefSize / 2);
            }
        }

        [Serializable]
        public class TopMiddleSizeMarker : Markers, ISizeMarker
        {
            public override int Index { get { return 2; } }

            public override Cursor Cursor { get { return Cursors.SizeNS; } }

            public override void UpdateLocation()
            {
                if (TargetFigure == null) return;
                var bounds = TargetFigure.Bounds;
                Location = new Point((int)Math.Round(bounds.Left + bounds.Width / 2),
                                     (int)Math.Round(bounds.Top) - DefSize / 2);
            }
        }

        [Serializable]
        public class TopRightSizeMarker : Markers, ISizeMarker
        {
            public override int Index { get { return 3; } }

            public override Cursor Cursor { get { return Cursors.SizeNESW; } }

            public override void UpdateLocation()
            {
                if (TargetFigure == null) return;
                var bounds = TargetFigure.Bounds;
                Location = new Point((int)Math.Round(bounds.Right) + DefSize / 2,
                                     (int)Math.Round(bounds.Top) - DefSize / 2);
            }
        }

        [Serializable]
        public class MiddleRightSizeMarker : Markers, ISizeMarker
        {
            public override int Index { get { return 4; } }

            public override Cursor Cursor { get { return Cursors.SizeWE; } }

            public override void UpdateLocation()
            {
                if (TargetFigure == null) return;
                var bounds = TargetFigure.Bounds;
                Location = new Point((int)Math.Round(bounds.Right) + DefSize / 2,
                                     (int)Math.Round(bounds.Top + bounds.Height / 2));
            }
        }

        [Serializable]
        public class BottomRightSizeMarker : Markers, ISizeMarker
        {
            public override int Index { get { return 5; } }

            public override Cursor Cursor { get { return Cursors.SizeNWSE; } }

            public override void UpdateLocation()
            {
                if (TargetFigure == null) return;
                var bounds = TargetFigure.Bounds;
                Location = new Point((int)Math.Round(bounds.Right) + DefSize / 2,
                                     (int)Math.Round(bounds.Bottom) + DefSize / 2);
            }
        }

        [Serializable]
        public class BottomMiddleSizeMarker : Markers, ISizeMarker
        {
            public override int Index { get { return 6; } }

            public override Cursor Cursor { get { return Cursors.SizeNS; } }

            public override void UpdateLocation()
            {
                if (TargetFigure == null) return;
                var bounds = TargetFigure.Bounds;
                Location = new Point((int)Math.Round(bounds.Left + bounds.Width / 2),
                                     (int)Math.Round(bounds.Bottom) + DefSize / 2);
            }
        }

        [Serializable]
        public class BottomLeftSizeMarker : Markers, ISizeMarker
        {
            public override int Index { get { return 7; } }

            public override Cursor Cursor { get { return Cursors.SizeNESW; } }

            public override void UpdateLocation()
            {
                if (TargetFigure == null) return;
                var bounds = TargetFigure.Bounds;
                Location = new Point((int)Math.Round(bounds.Left) - DefSize / 2,
                                     (int)Math.Round(bounds.Bottom) + DefSize / 2);
            }
        }

        [Serializable]
        public class MiddleLeftSizeMarker : Markers, ISizeMarker
        {
            public override int Index { get { return 8; } }

            public override Cursor Cursor { get { return Cursors.SizeWE; } }

            public override void UpdateLocation()
            {
                if (TargetFigure == null) return;
                var bounds = TargetFigure.Bounds;
                Location = new Point((int)Math.Round(bounds.Left) - DefSize / 2,
                                     (int)Math.Round(bounds.Top + bounds.Height / 2));
            }
        }

        [Serializable]
        public class VertexLocationMarker : Markers
        {
            private readonly int _index;

            public VertexLocationMarker(int index)
            {
                _index = index;
            }

            public override int Index { get { return _index; } }

            public override Cursor Cursor { get { return Cursors.SizeAll; } }

            public override void UpdateLocation()
            {
                if (TargetFigure == null) return;
                var points = TargetFigure.GetPoints();
                if (_index < 0 || _index >= points.Length) return;
                Location = new Point((int)points[_index].X, (int)points[_index].Y);
            }

            public override void Draw(Graphics gr)
            {
                gr.DrawEllipse(Pens.Black, Location.X - DefSize,
                    Location.Y - DefSize, DefSize * 2, DefSize * 2);
                gr.FillEllipse(Brushes.DarkOrange, Location.X - DefSize,
                    Location.Y - DefSize, DefSize * 2, DefSize * 2);
            }
        }


    }
}

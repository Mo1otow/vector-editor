﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectorEditor.src.controls
{

    public class NamedColor
    {
        public NamedColor(Color color, string colorName)
        {
            Color = color;
            Name = colorName;
        }

        public Color Color { get; set; }

        public string Name { get; set; }
    }
    public static class Utils
    {
        static private readonly List<NamedColor> NamedColors = new List<NamedColor>
            {   new NamedColor(Color.Black, "Black"),
            new NamedColor(Color.White, "White"),
            new NamedColor(Color.Red, "Red"),
            new NamedColor(Color.Lime, "Lime"),
            new NamedColor(Color.Blue, "Blue"),
            new NamedColor(Color.Yellow, "Yellow"),
            new NamedColor(Color.Magenta, "Magenta"),
            new NamedColor(Color.Cyan, "Cyan"),
            new NamedColor(Color.Brown, "Brown"),
            new NamedColor(Color.Green, "Green"),
            new NamedColor(Color.Navy, "Navy"),
            new NamedColor(Color.Olive, "Olive"),
            new NamedColor(Color.DarkMagenta, "Dark Magenta"),
            new NamedColor(Color.DarkCyan, "Dark Cyan"),
            new NamedColor(Color.WhiteSmoke, "Gray 10%"),
            new NamedColor(Color.Gainsboro, "Gray 25%"),
            new NamedColor(Color.LightGray, "Gray 40%"),
            new NamedColor(Color.Silver, "Gray 50%"),
            new NamedColor(Color.DarkGray, "Gray 60%"),
            new NamedColor(Color.Gray, "Gray 75%"),
            new NamedColor(Color.DimGray, "Gray 90%")
        };
        
        static private readonly List<Color> CustomColors = new List<Color>();
        //Стиль зарисовки
        static readonly HatchStyle[] HatchStyleArray = (HatchStyle[])Enum.GetValues(typeof(HatchStyle));

        static readonly int HatchStyleCount = HatchStyleArray.Length - 3;

        static readonly LinearGradientMode[] LinearGradientModeArray =
            (LinearGradientMode[])Enum.GetValues(typeof(LinearGradientMode));

        static readonly int LinearGradientModeCount = LinearGradientModeArray.Length;

        static readonly DashStyle[] DashStyleArray = (DashStyle[])Enum.GetValues(typeof(DashStyle));

        static readonly int DashStyleCount = DashStyleArray.Length - 1;
        public static object[] GetPenPatternNames()
        {
            var dashNameArray = Enum.GetNames(typeof(DashStyle));
            var n = 1 + DashStyleCount;
            var names = new object[n];
            names[0] = "No";
            var i = 1;
            for (var j = 0; j < DashStyleCount; j++) { names[i] = dashNameArray[j]; i++; }
            return names;
        }

        public static object[] GetLineJoinNames()
        {
            var names = new object[3];
            names[0] = "Corner";
            names[1] = "Beveled";
            names[2] = "Circular";
            return names;
        }

        public static object[] GetLineCapNames()
        {
            var names = new object[4];
            names[0] = "Flat";
            names[1] = "Square";
            names[2] = "Round";
            names[3] = "Triangular";
            return names;
        }

        public static bool FindColor(Color color)
        {
            var found = NamedColors.Any(t => t.Color == color);
            if (!found)
                if (CustomColors.Any(t => t == color))
                    found = true;
            return (found);
        }

        public static bool IsNamedColorIndex(int index)
        {
            return ((index >= 0) && (index < NamedColors.Count));
        }

        public static bool IsCustomColorIndex(int index)
        {
            return ((index >= NamedColors.Count) && (index < NamedColors.Count + CustomColors.Count));
        }

        public static Color ColorFromIndex(int index)
        {
            var color = new Color();
            if ((index >= 0) && (index < NamedColors.Count)) color = NamedColors[index].Color;
            else
            {
                var moreIndex = index - NamedColors.Count;
                if ((moreIndex >= 0) && (moreIndex < CustomColors.Count)) color = CustomColors[moreIndex];
            }
            return color;
        }

        public static int ColorToIndex(Color color)
        {
            var index = -1;
            for (var i = 0; i < NamedColors.Count; i++)
            {
                if (NamedColors[i].Color != color) continue;
                index = i; break;
            }
            if (index < 0)
            {
                for (var i = 0; i < CustomColors.Count; i++)
                {
                    if (CustomColors[i] != color) continue;
                    index = i + NamedColors.Count; break;
                }
            }
            return index;
        }

        public static int[] GetCustomColors()
        {
            var count = CustomColors.Count;
            var argbColors = new int[count];
            for (var i = 0; i < count; i++) argbColors[i] = CustomColors[i].ToArgb();
            return argbColors;
        }

        public static void AddCustomColor(Color color)
        {
            CustomColors.Add(color);
        }

        public static object[] GetAllColorNames()
        {
            var n = NamedColors.Count + CustomColors.Count;
            var names = new object[n];
            var nc = 0;
            foreach (var t in NamedColors)
                names[nc++] = t.Name;
            for (var i = 0; i < CustomColors.Count; i++)
                names[nc++] = String.Format(CultureInfo.InvariantCulture, "Color {0}", i);
            return names;
        }

        public static string GetColorNameFromIndex(int index)
        {
            var colorName = "";
            if (IsNamedColorIndex(index)) colorName = NamedColors[index].Name;
            else
                if (IsCustomColorIndex(index)) colorName =
                    String.Format(CultureInfo.InvariantCulture, "Color {0}",
                                  index - NamedColors.Count);
            return colorName;
        }

        public static string[] GetAllPatternNames()
        {
            var hatchNameArray = Enum.GetNames(typeof(HatchStyle));
            var linearGradientNameArray = Enum.GetNames(typeof(LinearGradientMode));

            var n = 2 + LinearGradientModeCount + HatchStyleCount;
            var names = new string[n];
            names[0] = "Transparent";
            names[1] = "Solid";
            var i = 2;
            for (var j = 0; j < LinearGradientModeCount; j++) { names[i] = linearGradientNameArray[j]; i++; }
            for (var j = 0; j < HatchStyleCount; j++) { names[i] = hatchNameArray[j]; i++; }
            return names;
        }

        public static bool IsNonePatternIndex(int index)
        {
            return (index == 0);
        }

        public static bool IsSolidPatternIndex(int index)
        {
            return (index == 1);
        }

        public static bool IsLinearGradientPatternIndex(int index)
        {
            checked
            {
                var idx = index - 2;
                return ((idx >= 0) && (idx < LinearGradientModeCount));
            }
        }

        public static bool IsHatchPatternIndex(int index)
        {
            checked
            {
                var idx = index - 2 - LinearGradientModeCount;
                return ((idx >= 0) && (idx < HatchStyleCount));
            }
        }

        public static HatchStyle HatchStyleFromIndex(int index)
        {
            checked
            {
                return (HatchStyle)(index - 2 - LinearGradientModeCount);
            }
        }

        public static LinearGradientMode LinearGradientModeFromIndex(int index)
        {
            checked
            {
                return (LinearGradientMode)(index - 2);
            }
        }



    }
}
